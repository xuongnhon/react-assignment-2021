import React from 'react';

interface ErrorBoundaryState {
  error: any,
  errorInfo: any
}

class ErrorBoundary extends React.Component {
  state: ErrorBoundaryState = {
    error: null,
    errorInfo: null,
  };

  componentDidCatch(error: any, errorInfo: any) {
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
  }

  render() {
    if (this.state.error) {
      return (
        <>
          <main className="container">
            <div className="row">
              <div className="col-6 offset-3 pt-3">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Something went wrong</h5>
                    <p className="text-danger">{this.state.error && this.state.error.toString()}</p>

                    <h5 className="card-title">Error Details:</h5>
                    <p className="text-danger">{this.state.errorInfo.componentStack}</p>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
