import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { AppContextProvider } from './AppContext';
import ErrorBoundary from './ErrorBoundary';
import InternalLayout from './views/layouts/InternalLayout';
import PublicLayout from './views/layouts/PublicLayout';
import PageNotFound from './views/PageNotFound';

class App extends React.Component {
  render() {
    return (
      <ErrorBoundary>
        <AppContextProvider>
          <BrowserRouter>
            <Switch>
              <Route path="/internal" component={InternalLayout} />
              <Route path="/public" component={PublicLayout} />
              <Route path="/not-found" component={PageNotFound} />
              <Redirect from="/" to="internal/dashboard" />
              <Redirect from="*" to="not-found" />
            </Switch>
          </BrowserRouter>
        </AppContextProvider>
      </ErrorBoundary>
    );
  }
}

export default App;
