import React, { Children } from 'react';

class AppDialog extends React.Component<any> {
  constructor(props: any) {
    super(props);
    this.renderChildren = this.renderChildren.bind(this);
  }

  renderChildren(key: string) {
    return Children.map(this.props.children, (child: any) => {
      if (child.key !== key) return;
      return child;
    });
  }

  closeDialog() {
    this.props.closeEvent();
  }

  render() {
    return (
      <>
        <div className={this.props.show ? "modal d-block" : "modal"}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                {this.renderChildren('header')}
                <button type="button" className="btn-close" onClick={() => this.closeDialog()}></button>
              </div>
              <div className="modal-body">
                {this.renderChildren('body')}
              </div>
              <div className="modal-footer">
                {this.renderChildren('footer')}
              </div>
            </div>
          </div>
        </div>

        {
          this.props.show && (
            <div className="modal-backdrop show"></div>
          )
        }
      </>
    );
  }
}

export default AppDialog;
