function Loading() {
  return (
    <>
      <div className="modal show d-block">
        <div className="modal-dialog modal-dialog-centered justify-content-center">
          <div className="spinner-border text-primary" style={{ height: '4rem', width: '4rem' }} role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      </div>
      <div className="modal-backdrop show bg-light"></div>
    </>
  );
}

export default Loading;
