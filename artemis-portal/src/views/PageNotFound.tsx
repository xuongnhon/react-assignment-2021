import React from 'react';
import Img404 from '../assets/images/404.png';

class PageNotFound extends React.Component<any> {
  backToHomePage() {
    this.props.history.push("/internal/dashboard");
  }

  render() {
    return (
      <div className="row">
        <div className="col-6 offset-3 pt-3">
          <div className="card border-0">
            <img src={Img404} className="card-img-top" alt="404" />
            <div className="card-body text-center">
              <p className="card-text">This is not the web page you are looking for.</p>
            </div>
            <div className="card-footer bg-transparent text-center border-0">
              <button type="button" className="btn btn-primary btn-sm" onClick={() => this.backToHomePage()}>
                <i className="fa fa-sign-in me-2" aria-hidden="true"></i>Back to home page
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PageNotFound;