import moment from 'moment';
import React from 'react';
import { AppContext } from '../../AppContext';
import { Schedule } from '../../models/Schedule';
import { User } from '../../models/User';

interface EditScheduleState {
  schedule: Schedule,
  fields: any,
  errors: any,
  rules: any,
}

class EditSchedule extends React.Component<any> {
  state: EditScheduleState = {
    schedule: {
      id: 0,
      title: "",
      creator: {
        id: 0,
        name: "",
        role: "",
      },
      description: "",
      location: "",
      startDate: new Date(),
      endDate: new Date()
    },
    fields: {
      title: "",
      description: "",
      creatorId: 0,
      location: "",
      startDate: "",
      endDate: ""
    },
    errors: {},
    rules: {
      title: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Title is required";
        }
      ],
      description: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Description is required";
        }
      ],
      creatorId: [
        (value: number) => {
          const isValid = value > 0;
          return isValid ? undefined : "Creator is required";
        }
      ],
      location: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Location is required";
        }
      ],
      startDate: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Start Date is required";
        }
      ],
      endDate: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "End date is required";
        },
        (value: string) => {
          const startDate = new Date(this.state.fields.startDate);
          const endDate = new Date(value);
          const isValid = endDate.getTime() - startDate.getTime() > 0;
          return isValid ? undefined : "End Date must be greater than Start Date";
        }
      ]
    },
  };

  back() {
    this.props.history.push('/internal/schedules');
  }

  async handleChange(e: any, fieldName: string) {
    const value = e.target.value;
    const newFields = {
      ...this.state.fields
    };
    newFields[fieldName] = value;
    await this.setState({ fields: newFields });

    this.handleValidation(fieldName);
  }

  async handleValidation(fieldName: string) {
    const value = this.state.fields[fieldName];
    const rules = this.state.rules[fieldName];

    let errorMessage = undefined;
    const newErrors = {
      ...this.state.errors
    };
    for (let i = 0; i < rules.length; i++) {
      const rule = rules[i];

      errorMessage = rule(value);
      if (errorMessage) {
        newErrors[fieldName] = errorMessage;
        await this.setState({ errors: newErrors });
        return;
      }
    }

    delete newErrors[fieldName];
    await this.setState({ errors: newErrors });
  }

  async handleUpdate() {
    await this.handleValidation("title");
    await this.handleValidation("description");
    await this.handleValidation("creatorId");
    await this.handleValidation("location");
    await this.handleValidation("startDate");
    await this.handleValidation("endDate");

    if (this.state.errors.title ||
      this.state.errors.description ||
      this.state.errors.creatorId ||
      this.state.errors.location ||
      this.state.errors.startDate ||
      this.state.errors.endDate
    ) {
      return;
    }

    const { users, dispatchSchedules } = this.context;
    const creator = users.find((user: User) => user.id === Number(this.state.fields.creatorId));
    dispatchSchedules({
      type: "EDIT_SCHEDULE",
      schedule: {
        id: this.state.schedule.id,
        title: this.state.fields.title,
        description: this.state.fields.description,
        creator: { ...creator },
        location: this.state.fields.location,
        startDate: this.state.fields.startDate,
        endDate: this.state.fields.endDate
      }
    });

    this.props.history.push('/internal/schedules');
  }

  componentDidMount() {
    const { schedules } = this.context;
    const id = this.props.match.params.id;
    const schedule = schedules.find((schedule: Schedule) => schedule.id === Number(id));

    this.setState({
      schedule: schedule,
      fields: {
        title: schedule.title,
        description: schedule.description,
        creatorId: schedule.creator.id,
        location: schedule.location,
        startDate: moment(schedule.startDate).format("yyyy-MM-DDThh:mm"),
        endDate: moment(schedule.endDate).format("yyyy-MM-DDThh:mm")
      }
    });
  }

  render() {
    const { users } = this.context;

    return (
      <>
        <div className="row mb-3">
          <div className="col-12">
            <button type="button" className="btn btn-danger" onClick={() => this.back()}>
              <i className="fa fa-arrow-left me-3" aria-hidden="true"></i>Back
            </button>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <ul className="nav nav-tabs mb-3" role="tablist">
              <li className="nav-item" role="presentation">
                <button className="nav-link active" data-bs-toggle="tab" type="button" role="tab" aria-controls="edit"
                  aria-selected="false">Edit</button>
              </li>
            </ul>
            <div className="tab-content">
              <div className="tab-pane fade show active" role="tabpanel" aria-labelledby="edit-tab">
                <div className="bg-light p-3">
                  <form>
                    <div className="mb-3">
                      <label className="form-label">Title</label>
                      <input name="title" placeholder="Title"
                        className={this.state.errors.title ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "title")}
                        onBlur={() => this.handleValidation("title")}
                        value={this.state.fields.title}
                      />
                      {this.state.errors.title && (
                        <div className="invalid-feedback">
                          {this.state.errors.title}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Description</label>
                      <input name="description" placeholder="Description"
                        className={this.state.errors.description ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "description")}
                        onBlur={() => this.handleValidation("description")}
                        value={this.state.fields.description}
                      />
                      {this.state.errors.description && (
                        <div className="invalid-feedback">
                          {this.state.errors.description}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Creator</label>
                      <select name="creatorId"
                        className={this.state.errors.creatorId ? "form-select is-invalid" : "form-select"}
                        onChange={(e) => this.handleChange(e, "creatorId")}
                        onBlur={() => this.handleValidation("creatorId")}
                        value={this.state.fields.creatorId}
                      >
                        {
                          users.map((user: User) => (
                            <option key={user.id} value={user.id} defaultValue={this.state.fields.creatorId}>{user.name}</option>
                          ))
                        }
                      </select>
                      {this.state.errors.creatorId && (
                        <div className="invalid-feedback">
                          {this.state.errors.creatorId}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Location</label>
                      <input name="location" placeholder="Location"
                        className={this.state.errors.location ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "location")}
                        onBlur={() => this.handleValidation("location")}
                        value={this.state.fields.location}
                      />
                      {this.state.errors.location && (
                        <div className="invalid-feedback">
                          {this.state.errors.location}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Start date</label>
                      <input type="datetime-local" name="startDate" placeholder="Start Date"
                        className={this.state.errors.startDate ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "startDate")}
                        onBlur={() => this.handleValidation("startDate")}
                        value={this.state.fields.startDate}
                      />
                      {this.state.errors.startDate && (
                        <div className="invalid-feedback">
                          {this.state.errors.startDate}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">End date</label>
                      <input type="datetime-local" name="endDate" placeholder="End Date"
                        className={this.state.errors.endDate ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "endDate")}
                        onBlur={() => this.handleValidation("endDate")}
                        value={this.state.fields.endDate}
                      />
                      {this.state.errors.endDate && (
                        <div className="invalid-feedback">
                          {this.state.errors.endDate}
                        </div>
                      )}
                    </div>
                    <div className="clearfix">
                      <div className="float-end">
                        <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.handleUpdate()}>
                          <i className="fa fa-save me-2" aria-hidden="true"></i>Update
                        </button>
                        <button type="button" className="btn btn-secondary btn-sm" onClick={() => this.back()}>
                          <i className="fa fa-times me-2" aria-hidden="true"></i>Cancel
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

EditSchedule.contextType = AppContext;

export default EditSchedule;
