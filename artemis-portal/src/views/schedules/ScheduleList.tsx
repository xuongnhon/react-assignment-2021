import React from 'react';
import moment from 'moment';
import { AppContext } from '../../AppContext';
import { Schedule } from '../../models/Schedule';
import AppDialog from '../../components/AppDialog';

interface ScheduleListState {
  selectedSchedule: Schedule | undefined | null
  showDialog: boolean
}

class ScheduleList extends React.Component<any> {
  state: ScheduleListState = {
    selectedSchedule: undefined,
    showDialog: false
  };

  editSchedule(schedule: Schedule) {
    this.props.history.push(`/internal/schedules/edit/${schedule.id}`);
  }

  showDetail(schedule: Schedule) {
    this.setState({ selectedSchedule: schedule, showDialog: true });
  }

  closeDialog() {
    this.setState({ showDialog: false });
  }

  handleDelete(schedule: Schedule) {
    const continute = window.confirm('Are you sure you want to delete?');

    if (continute) {
      const { dispatchSchedules } = this.context;
      dispatchSchedules({
        type: "DELETE_SCHEDULE",
        id: schedule.id
      });
    }
  }

  render() {
    const { schedules } = this.context;

    return (
      <>
        <div className="col-12">
          <table className="table">
            <thead>
              <tr>
                <th>
                  <i className="fa fa-text-width me-2" aria-hidden="true"></i>Title
                </th>
                <th>
                  <i className="fa fa-user me-2" aria-hidden="true"></i>Creator
                </th>
                <th>
                  <i className="fa fa-paragraph me-2" aria-hidden="true"></i>Description
                </th>
                <th>
                  <i className="fa fa-map-marker" aria-hidden="true"></i>
                </th>
                <th>
                  <i className="fa fa-calendar-o me-2" aria-hidden="true"></i>Time Start
                </th>
                <th>
                  <i className="fa fa-calendar-o me-2" aria-hidden="true"></i>Time End
                </th>
                <th>
                </th>
              </tr>
            </thead>
            <tbody>
              {
                schedules.map((schedule: Schedule) => (
                  <tr key={schedule.id}>
                    <td className="align-middle">{schedule.title}</td>
                    <td className="align-middle">{schedule.creator.name}</td>
                    <td className="align-middle">{schedule.description}</td>
                    <td className="align-middle">{schedule.location}</td>
                    <td className="align-middle">{moment(schedule.startDate).format("MMM DD, yyyy hh:mm a")}</td>
                    <td className="align-middle">{moment(schedule.endDate).format("MMM DD, yyyy hh:mm a")}</td>
                    <td>
                      <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.showDetail(schedule)}>
                        <i className="fa fa-info me-2" aria-hidden="true"></i>Detail
                      </button>
                      <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.editSchedule(schedule)}>
                        <i className="fa fa-edit me-2" aria-hidden="true"></i>Edit
                      </button>
                      <button type="button" className="btn btn-danger btn-sm" onClick={() => this.handleDelete(schedule)}>
                        <i className="fa fa-trash me-2" aria-hidden="true"></i>Delete
                      </button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
        {
          this.state.selectedSchedule && (
            <AppDialog show={this.state.showDialog} closeEvent={() => this.closeDialog()}>
              <h5 key="header" className="modal-title">Schedule Detail</h5>
              <div key="body">
                <div className="mb-3">
                  <label className="form-label">Title</label>
                  <input className="form-control" name="title" placeholder="Title" value={this.state.selectedSchedule.title} disabled />
                </div>
                <div className="mb-3">
                  <label className="form-label">Description</label>
                  <input className="form-control" name="description" placeholder="Description" value={this.state.selectedSchedule.description}
                    disabled />
                </div>
                <div className="mb-3">
                  <label className="form-label">Creator</label>
                  <input className="form-control" name="creator" placeholder="creator" value={this.state.selectedSchedule.creator.name}
                    disabled />
                </div>
                <div className="mb-3">
                  <label className="form-label">Location</label>
                  <input className="form-control" name="location" placeholder="Location" value={this.state.selectedSchedule.location}
                    disabled />
                </div>
                <div className="mb-3">
                  <label className="form-label">Start date</label>
                  <input className="form-control" type="datetime-local" name="startDate" placeholder="Start Date" value={moment(this.state.selectedSchedule.startDate).format("yyyy-MM-DDThh:mm")}
                    disabled />
                </div>
                <div className="mb-3">
                  <label className="form-label">End date</label>
                  <input className="form-control" type="datetime-local" name="endDate" placeholder="End Date" value={moment(this.state.selectedSchedule.endDate).format("yyyy-MM-DDThh:mm")}
                    disabled />
                </div>
              </div>
              <div key="footer">
                <button type="button" className="btn btn-secondary" onClick={() => this.closeDialog()}>Close</button>
              </div>
            </AppDialog>
          )
        }
      </>
    );
  }
}

ScheduleList.contextType = AppContext;

export default ScheduleList;
