import React from 'react';
import { AppContext } from '../AppContext';
import { User } from '../models/User';

interface LoginState {
  fields: any,
  errors: any,
  rules: any,
  errorMessage: string
}

class Login extends React.Component<any> {
  state: LoginState = {
    fields: {
      username: "",
      password: ""
    },
    errors: {},
    rules: {
      username: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Username is required";
        }
      ],
      password: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Password is required";
        }
      ]
    },
    errorMessage: ""
  };

  async handleChange(e: any, fieldName: string) {
    const value = e.target.value;
    const newFields = {
      ...this.state.fields
    };
    newFields[fieldName] = value;
    await this.setState({ fields: newFields });

    this.handleValidation(fieldName);
  }

  async handleValidation(fieldName: string) {
    const value = this.state.fields[fieldName];
    const rules = this.state.rules[fieldName];

    let errorMessage = undefined;
    const newErrors = {
      ...this.state.errors
    };
    for (let i = 0; i < rules.length; i++) {
      const rule = rules[i];

      errorMessage = rule(value);
      if (errorMessage) {
        newErrors[fieldName] = errorMessage;
        await this.setState({ errors: newErrors });
        return;
      }
    }

    delete newErrors[fieldName];
    await this.setState({ errors: newErrors });
  }

  async handleLogin() {
    await this.handleValidation("username");
    await this.handleValidation("password");

    if (this.state.errors.username ||
      this.state.errors.password
    ) {
      return;
    }

    const { users, dispatchAuthenticationModel } = this.context;
    const user = users.find((user: User) => user.username === this.state.fields.username
      && user.password === this.state.fields.password);

    if (user) {
      dispatchAuthenticationModel({
        type: "LOGIN",
        user: {
          ...user
        }
      });

      this.props.history.push('/internal/dashboard');
    } else {
      await this.setState({ errorMessage: "Invalid username or password" });
    }
  }

  render() {
    return (
      <>
        <div className="row">
          <div className="col-6 offset-3 pt-3">
            <div className="card">
              <div className="card-body">
                {this.state.errorMessage && (
                  <div className="alert alert-danger" role="alert">
                    {this.state.errorMessage}
                  </div>
                )}

                <form>
                  <div className="mb-3">
                    <label className="form-label">Username</label>
                    <input name="username" placeholder="Username"
                      className={this.state.errors.username ? "form-control is-invalid" : "form-control"}
                      onChange={(e) => this.handleChange(e, "username")}
                      onBlur={() => this.handleValidation("username")}
                      value={this.state.fields.username}
                    />
                    {this.state.errors.username && (
                      <div className="invalid-feedback">
                        {this.state.errors.username}
                      </div>
                    )}
                  </div>
                  <div className="mb-3">
                    <label className="form-label">Password</label>
                    <input type="password" name="password" placeholder="Password"
                      className={this.state.errors.password ? "form-control is-invalid" : "form-control"}
                      onChange={(e) => this.handleChange(e, "password")}
                      onBlur={() => this.handleValidation("password")}
                      value={this.state.fields.password}
                    />
                    {this.state.errors.password && (
                      <div className="invalid-feedback">
                        {this.state.errors.password}
                      </div>
                    )}
                  </div>
                  <div className="clearfix">
                    <div className="float-end">
                      <button type="button" className="btn btn-primary" onClick={() => this.handleLogin()}>
                        Login
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

Login.contextType = AppContext;

export default Login;
