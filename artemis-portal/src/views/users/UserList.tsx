import React from 'react';
import { AppContext } from '../../AppContext';
import { User } from '../../models/User';
import UserDetail from './UserDetail';

class UserList extends React.Component<any> {
  addNewUser() {
    this.props.history.push('/internal/users/create');
  }

  render() {
    const { users } = this.context;

    return (
      <>
        <div>
          <div className="row mb-3">
            <div className="col-12">
              <button type="button" className="btn btn-primary" onClick={() => this.addNewUser()}>
                <i className="fa fa-plus-square me-2" aria-hidden="true"></i>Add
              </button>
            </div>
          </div>

          <div className="row">
            {
              users.map((user: User) => (
                <div key={user.id} className="col-3">
                  <UserDetail user={user} />
                </div>
              ))
            }
          </div>
        </div>
      </>
    );
  }
}

UserList.contextType = AppContext;

export default UserList;
