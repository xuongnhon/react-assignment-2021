import React from 'react';
import { AppContext } from '../../AppContext';

interface CreateUserState {
  fields: any,
  errors: any,
  rules: any,
}

class CreateUser extends React.Component<any> {
  state: CreateUserState = {
    fields: {
      name: "",
      role: "",
      username: "",
      password: ""
    },
    errors: {},
    rules: {
      name: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Name is required";
        }
      ],
      role: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Role is required";
        }
      ],
      username: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Username is required";
        },
        (value: string) => {
          const isValid = value.length > 6;
          return isValid ? undefined : "Username must have at least 6 characters";
        }
      ],
      password: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Password is required";
        }
      ]
    },
  };

  back() {
    this.props.history.push('/internal/users');
  }

  async handleChange(e: any, fieldName: string) {
    const value = e.target.value;
    const newFields = {
      ...this.state.fields
    };
    newFields[fieldName] = value;
    await this.setState({ fields: newFields });

    this.handleValidation(fieldName);
  }

  async handleValidation(fieldName: string) {
    const value = this.state.fields[fieldName];
    const rules = this.state.rules[fieldName];

    let errorMessage = undefined;
    const newErrors = {
      ...this.state.errors
    };
    for (let i = 0; i < rules.length; i++) {
      const rule = rules[i];

      errorMessage = rule(value);
      if (errorMessage) {
        newErrors[fieldName] = errorMessage;
        await this.setState({ errors: newErrors });
        return;
      }
    }

    delete newErrors[fieldName];
    await this.setState({ errors: newErrors });
  }

  async handleSave() {
    await this.handleValidation("name");
    await this.handleValidation("role");
    await this.handleValidation("username");
    await this.handleValidation("password");

    if (this.state.errors.name ||
      this.state.errors.role ||
      this.state.errors.username ||
      this.state.errors.password
    ) {
      return;
    }

    const { dispatchUsers } = this.context;
    dispatchUsers({
      type: "ADD_USER",
      user: {
        name: this.state.fields.name,
        role: this.state.fields.role,
        username: this.state.fields.username,
        password: this.state.fields.password
      }
    });

    this.props.history.push('/internal/users');
  }

  render() {
    return (
      <>
        <div className="row mb-3">
          <div className="col-12">
            <button type="button" className="btn btn-danger" onClick={() => this.back()}>
              <i className="fa fa-arrow-left me-3" aria-hidden="true"></i>Back
            </button>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <ul className="nav nav-tabs mb-3" role="tablist">
              <li className="nav-item" role="presentation">
                <button className="nav-link active" data-bs-toggle="tab" type="button" role="tab" aria-controls="add"
                  aria-selected="false">Add</button>
              </li>
            </ul>
            <div className="tab-content">
              <div className="tab-pane fade show active" role="tabpanel" aria-labelledby="add-tab">
                <div className="bg-light p-3">
                  <form>
                    <div className="mb-3">
                      <label className="form-label">Name</label>
                      <input name="name" placeholder="Name"
                        className={this.state.errors.name ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "name")}
                        onBlur={() => this.handleValidation("name")}
                        value={this.state.fields.name}
                      />
                      {this.state.errors.name && (
                        <div className="invalid-feedback">
                          {this.state.errors.name}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Role</label>
                      <input name="role" placeholder="Role"
                        className={this.state.errors.role ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "role")}
                        onBlur={() => this.handleValidation("role")}
                        value={this.state.fields.role}
                      />
                      {this.state.errors.role && (
                        <div className="invalid-feedback">
                          {this.state.errors.role}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Username</label>
                      <input name="username" placeholder="Username"
                        className={this.state.errors.username ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "username")}
                        onBlur={() => this.handleValidation("username")}
                        value={this.state.fields.username}
                      />
                      {this.state.errors.username && (
                        <div className="invalid-feedback">
                          {this.state.errors.username}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Password</label>
                      <input type="password" name="password" placeholder="Password"
                        className={this.state.errors.password ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "password")}
                        onBlur={() => this.handleValidation("password")}
                        value={this.state.fields.password}
                      />
                      {this.state.errors.password && (
                        <div className="invalid-feedback">
                          {this.state.errors.password}
                        </div>
                      )}
                    </div>
                    <div className="clearfix">
                      <div className="float-end">
                        <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.handleSave()}>
                          <i className="fa fa-save me-2" aria-hidden="true"></i>Save
                        </button>
                        <button type="button" className="btn btn-secondary btn-sm" onClick={() => this.back()}>
                          <i className="fa fa-times me-2" aria-hidden="true"></i>Cancel
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

CreateUser.contextType = AppContext;

export default CreateUser;
