import React from 'react';
import { withRouter } from 'react-router-dom';
import { AppContext } from '../../AppContext';
import { User } from '../../models/User';
import UserLogo from '../../assets/images/user.svg';

interface UserDetailState {
  user: User,
  fields: any,
  errors: any,
  rules: any,
  editMode: boolean
}

class UserDetail extends React.Component<any> {
  state: UserDetailState = {
    user: this.props.user,
    fields: {
      name: "",
      role: "",
    },
    errors: {},
    rules: {
      name: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Name is required";
        }
      ],
      role: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Role is required";
        }
      ]
    },
    editMode: false
  };

  switchToEditMode() {
    this.setState({
      fields: {
        name: this.state.user.name,
        role: this.state.user.role
      },
      editMode: true
    });
  }

  switchToViewMode() {
    this.setState({ editMode: false });
  }

  async handleChange(e: any, fieldName: string) {
    const value = e.target.value;
    const newFields = {
      ...this.state.fields
    };
    newFields[fieldName] = value;
    await this.setState({ fields: newFields });

    this.handleValidation(fieldName);
  }

  async handleValidation(fieldName: string) {
    const value = this.state.fields[fieldName];
    const rules = this.state.rules[fieldName];

    let errorMessage = undefined;
    const newErrors = {
      ...this.state.errors
    };
    for (let i = 0; i < rules.length; i++) {
      const rule = rules[i];

      errorMessage = rule(value);
      if (errorMessage) {
        newErrors[fieldName] = errorMessage;
        await this.setState({ errors: newErrors });
        return;
      }
    }

    delete newErrors[fieldName];
    await this.setState({ errors: newErrors });
  }

  createSchedule() {
    this.props.history.push(`/internal/users/${this.state.user.id}/create-schedule`);
  }

  async handleUpdate() {
    await this.handleValidation("name");
    await this.handleValidation("role");

    if (this.state.errors.name && this.state.errors.role) {
      return;
    }

    const { dispatchUsers } = this.context;
    dispatchUsers({
      type: "EDIT_USER",
      user: {
        id: this.state.user.id,
        name: this.state.fields.name,
        role: this.state.fields.role
      }
    });

    this.switchToViewMode();
  }

  handleDelete() {
    const continute = window.confirm('Are you sure you want to delete?');

    if (continute) {
      const { dispatchUsers } = this.context;
      dispatchUsers({
        type: "DELETE_USER", id: this.state.user.id
      });
    }
  }

  render() {
    return (
      <>
        <form>
          <div className="card rounded-0">
            <div className="card-header bg-primary text-white text-center rounded-0">
              {
                this.state.editMode
                  ? (
                    <>
                      <input name="name" placeholder="Name"
                        className={this.state.errors.name ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "name")}
                        onBlur={() => this.handleValidation("name")}
                        value={this.state.fields.name}
                      />
                      {this.state.errors.name && (
                        <div className="invalid-feedback bg-white">
                          {this.state.errors.name}
                        </div>
                      )}
                    </>
                  )
                  : (
                    <>
                      <i className="fa fa-user me-2" aria-hidden="true"></i>{this.state.user.name}
                    </>
                  )
              }
            </div>
            <img src={UserLogo} className="img-fluid px-5 py-3" alt="user" />
            <div className="card-body d-flex flex-column pt-0">
              {
                this.state.editMode
                  ? (
                    <>
                      <input name="role" placeholder="Role"
                        className={this.state.errors.role ? "form-control is-invalid mb-2" : "form-control mb-2"}
                        onChange={(e) => this.handleChange(e, "role")}
                        onBlur={() => this.handleValidation("role")}
                        value={this.state.fields.role}
                      />
                      {this.state.errors.role && (
                        <div className="invalid-feedback bg-white mb-2">
                          {this.state.errors.role}
                        </div>
                      )}
                    </>

                  )
                  : (
                    <div className="text-center mb-1">{this.state.user.role}</div>
                  )
              }
              <div className="text-center">
                <button type="button" className="btn btn-success btn-sm" onClick={() => this.createSchedule()}>
                  <i className="fa fa-calendar me-2" aria-hidden="true"></i>Schedules
                </button>
              </div>
            </div>
            <div className="card-footer bg-transparent text-center">
              {
                this.state.editMode
                  ? (
                    <>
                      <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.handleUpdate()}>
                        <i className="fa fa-edit me-2" aria-hidden="true"></i>Update
                      </button>
                      <button type="button" className="btn btn-secondary btn-sm" onClick={() => this.switchToViewMode()}>
                        <i className="fa fa-times me-2" aria-hidden="true"></i>Cancel
                      </button>
                    </>
                  )
                  : (
                    <>
                      <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.switchToEditMode()}>
                        <i className="fa fa-edit me-2" aria-hidden="true"></i>Edit
                      </button>
                      <button type="button" className="btn btn-danger btn-sm" onClick={() => this.handleDelete()}>
                        <i className="fa fa-trash me-2" aria-hidden="true"></i>Remove
                      </button>
                    </>
                  )
              }
            </div>
          </div>
        </form>
      </>
    );
  }
}

UserDetail.contextType = AppContext;

export default withRouter(UserDetail);
