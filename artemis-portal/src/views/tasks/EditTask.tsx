import React from 'react';
import { AppContext } from '../../AppContext';
import { TaskStatus } from '../../enums/TaskStatus';
import { Task } from '../../models/Task';

interface EditTaskState {
  task: Task | undefined | null,
  fields: any,
  errors: any,
  rules: any
}

class EditTask extends React.Component<any> {
  state: EditTaskState = {
    task: undefined,
    fields: {
      title: "",
      description: "",
      status: TaskStatus.New
    },
    errors: {},
    rules: {
      title: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Title is required";
        }
      ],
      description: [
        (value: string) => {
          const isValid = value !== undefined
            &&
            value !== null
            && value.trim() !== "";
          return isValid ? undefined : "Description is required";
        }
      ]
    }
  };

  back() {
    this.props.history.push('/internal/tasks');
  }

  async handleChange(e: any, fieldName: string) {
    const value = e.target.value;
    const newFields = {
      ...this.state.fields
    };
    newFields[fieldName] = value;
    await this.setState({ fields: newFields });

    this.handleValidation(fieldName);
  }

  async handleValidation(fieldName: string) {
    const value = this.state.fields[fieldName];
    const rules = this.state.rules[fieldName];

    let errorMessage = undefined;
    const newErrors = {
      ...this.state.errors
    };
    for (let i = 0; i < rules.length; i++) {
      const rule = rules[i];

      errorMessage = rule(value);
      if (errorMessage) {
        newErrors[fieldName] = errorMessage;
        await this.setState({ errors: newErrors });
        return;
      }
    }

    delete newErrors[fieldName];
    await this.setState({ errors: newErrors });
  }

  handleTaskStatusChange(e: any) {
    this.setState({ fields: { ...this.state.fields, status: Number(e.target.value) } });
  }

  async handleSave() {
    await this.handleValidation("title");
    await this.handleValidation("description")

    if (this.state.errors.title || this.state.errors.description) {
      return;
    }

    const { dispatchTasks } = this.context;
    dispatchTasks({
      type: "EDIT_TASK",
      task: {
        id: this.state.task?.id,
        title: this.state.fields.title,
        description: this.state.fields.description,
        status: this.state.fields.status
      }
    });

    this.props.history.push('/internal/tasks');
  }

  handleDelete() {
    const continute = window.confirm('Are you sure you want to delete?');

    if (continute) {
      const { dispatchTasks } = this.context;
      dispatchTasks({
        type: "DELETE_TASK", id: this.state.task?.id
      });

      this.props.history.push('/internal/tasks');
    }
  }

  componentDidMount() {
    const { tasks } = this.context;
    const id = this.props.match.params.id;
    const task = tasks.find((task: Task) => task.id === Number(id));

    this.setState({
      task: task,
      fields: {
        title: task.title,
        description: task.description,
        status: task.status
      }
    });
  }

  render() {
    return (
      <>
        <div className="row mb-3">
          <div className="col-12">
            <button type="button" className="btn btn-danger" onClick={() => this.back()}>
              <i className="fa fa-arrow-left me-3" aria-hidden="true"></i>Back
            </button>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <ul className="nav nav-tabs mb-3" role="tablist">
              <li className="nav-item" role="presentation">
                <button className="nav-link" data-bs-toggle="tab" data-bs-target="#view" type="button" role="tab"
                  aria-controls="view" aria-selected="true">View</button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#edit" type="button" role="tab"
                  aria-controls="edit" aria-selected="false">Edit</button>
              </li>
            </ul>
            <div className="tab-content">
              <div className="tab-pane fade" id="view" role="tabpanel" aria-labelledby="view-tab">
                <div className="bg-light p-3">
                  {this.state.task && (
                    <>
                      <h1>{this.state.task.title}</h1>
                      <p>{this.state.task.description}</p>
                    </>
                  )}
                </div>
              </div>
              <div className="tab-pane fade show active" id="edit" role="tabpanel" aria-labelledby="edit-tab">
                <div className="bg-light p-3">
                  <form>
                    <div className="mb-3">
                      <label className="form-label">Title</label>
                      <input name="title" placeholder="Title"
                        className={this.state.errors.title ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "title")}
                        onBlur={() => this.handleValidation("title")}
                        value={this.state.fields.title}
                      />
                      {this.state.errors.title && (
                        <div className="invalid-feedback">
                          {this.state.errors.title}
                        </div>
                      )}
                    </div>
                    <div className="mb-3">
                      <textarea rows={3} name="Description" placeholder="Description"
                        className={this.state.errors.description ? "form-control is-invalid" : "form-control"}
                        onChange={(e) => this.handleChange(e, "description")}
                        onBlur={() => this.handleValidation("description")}
                        value={this.state.fields.description}
                      ></textarea>
                      {this.state.errors.description && (
                        <div className="invalid-feedback">
                          {this.state.errors.description}
                        </div>
                      )}
                    </div>
                    <div className="bg-dark mb-3 p-2 text-white">
                      Status
                    </div>
                    <div className="mb-3">
                      <input className="form-check-input me-2" type="radio" value={TaskStatus.New} name="status"
                        checked={this.state.fields.status === TaskStatus.New}
                        onChange={(e) => this.handleTaskStatusChange(e)}
                      />
                      <label className="form-check-label me-2">
                        New
                      </label>
                      <input className="form-check-input me-2" type="radio" value={TaskStatus.InProgress} name="status"
                        checked={this.state.fields.status === TaskStatus.InProgress}
                        onChange={(e) => this.handleTaskStatusChange(e)}
                      />
                      <label className="form-check-label me-2">
                        In Progress
                      </label>
                      <input className="form-check-input me-2" type="radio" value={TaskStatus.Done} name="status"
                        checked={this.state.fields.status === TaskStatus.Done}
                        onChange={(e) => this.handleTaskStatusChange(e)}
                      />
                      <label className="form-check-label">
                        Done
                      </label>
                    </div>
                    <div className="clearfix">
                      <div className="float-end">
                        <button type="button" className="btn btn-primary btn-sm me-3" onClick={() => this.handleSave()}>
                          <i className="fa fa-save me-2" aria-hidden="true"></i>Save
                        </button>
                        <button type="button" className="btn btn-danger btn-sm me-3" onClick={() => this.handleDelete()}>
                          <i className="fa fa-save me-2" aria-hidden="true"></i>Delete
                        </button>
                        <button type="button" className="btn btn-secondary btn-sm" onClick={() => this.back()}>
                          <i className="fa fa-times me-2" aria-hidden="true"></i>Cancel
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

EditTask.contextType = AppContext;

export default EditTask;
