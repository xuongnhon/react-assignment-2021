import React from 'react';
import { AppContext } from '../../AppContext';
import { Task } from '../../models/Task';
import { TaskStatus } from '../../enums/TaskStatus';

interface TaskListState {
  selectedTask: Task | undefined | null
}

class TaskList extends React.Component<any> {
  state: TaskListState = {
    selectedTask: undefined
  };

  addNewTask() {
    this.props.history.push('/internal/tasks/create');
  }

  editTask(id: number) {
    this.props.history.push(`/internal/tasks/edit/${id}`);
  }

  viewTask(task: Task) {
    this.setState({ selectedTask: task });
  }

  closeTaskDetailPanel() {
    this.setState({ selectedTask: undefined });
  }

  render() {
    const { tasks } = this.context;
    const newTasks = tasks.filter((task: Task) => task.status === TaskStatus.New);
    const inProgressTasks = tasks.filter((task: Task) => task.status === TaskStatus.InProgress);
    const doneTasks = tasks.filter((task: Task) => task.status === TaskStatus.Done);

    return (
      <>
        <div className="row">
          <div className="col-4">
            <div className="card rounded-0">
              <div className="card-body bg-secondary text-white">
                <h5 className="mb-0 float-start">New Tasks</h5>
                <i className="fa fa-tasks float-end" aria-hidden="true"></i>
              </div>
              {
                newTasks.length > 0 && (
                  <ul className="list-group list-group-flush">
                    {
                      newTasks.map((task: Task) => (
                        <li key={task.id} onClick={() => this.editTask(task.id)} className="list-group-item show-pointer" title="Click to edit">
                          {task.title}
                        </li>
                      ))
                    }
                  </ul>
                )
              }
              <div className="card-body">
                <button type="button" onClick={() => this.addNewTask()} className="btn btn-primary btn-sm">
                  <i className="fa fa-plus-square me-2" aria-hidden="true"></i>Add
                </button>
              </div>
            </div>
          </div>

          <div className="col-4">
            <div className="card rounded-0">
              <div className="card-body bg-success text-white">
                <h5 className="mb-0 float-start">Tasks in progress</h5>
                <i className="fa fa-spinner float-end" aria-hidden="true"></i>
              </div>
              {
                inProgressTasks.length > 0 && (
                  <ul className="list-group list-group-flush">
                    {
                      inProgressTasks.map((task: Task) => (
                        <li key={task.id} onClick={() => this.editTask(task.id)} className="list-group-item show-pointer" title="Click to edit">
                          {task.title}
                        </li>
                      ))
                    }
                  </ul>
                )
              }
            </div>
          </div>

          <div className="col-4">
            <div className="card rounded-0">
              <div className="card-body bg-danger text-white">
                <h5 className="mb-0 float-start">Tasks got done</h5>
                <i className="fa fa-modx float-end" aria-hidden="true"></i>
              </div>
              {
                doneTasks.length > 0 && (
                  <ul className="list-group list-group-flush">
                    {
                      doneTasks.map((task: Task) => (
                        <li key={task.id} onClick={() => this.viewTask(task)} className="list-group-item show-pointer" title="Click to view">
                          {task.title}
                        </li>
                      ))
                    }
                  </ul>
                )
              }
              {
                this.state.selectedTask && (
                  <div className="card-body">
                    <div className="d-flex justify-content-end">
                      <button type="button" onClick={() => this.closeTaskDetailPanel()} className="btn-close" aria-label="Close"></button>
                    </div>
                    <div>
                      <div className="mb-3">
                        <h5>{this.state.selectedTask.title + " - Detail View"}</h5>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">Title</label>
                        <input className="form-control" name="title" placeholder="Title" value={this.state.selectedTask.title} disabled />
                      </div>
                      <div className="mb-3">
                        <label className="form-label">Description</label>
                        <textarea className="form-control" rows={3} name="Description" placeholder="Description" value={this.state.selectedTask.description} disabled></textarea>
                      </div>
                    </div>
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </>
    );
  }
}

TaskList.contextType = AppContext;

export default TaskList;
