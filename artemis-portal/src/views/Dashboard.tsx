import React from 'react';
import { AppContext } from '../AppContext';

class Dashboard extends React.Component {
  render() {
    const { authenticationModel } = this.context;

    return (
      <div className="row mb-3">
        <div className="col-12">
          <div className="alert alert-success alert-dismissible fade show" role="alert">
            Welcome <strong>{authenticationModel.loggedUser.name}</strong>
            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.contextType = AppContext;

export default Dashboard;
