import React from 'react';

class Logout extends React.Component {
  login() {

  }

  async render() {
    return (
      <div className="row">
        <div className="col-6 offset-3 pt-3">
          <div className="card border-0">
            <img src=".\assets\images\logout.png" className="card-img-top" />
            <div className="card-body text-center">
              <p className="card-text">You have been logged out successfully.</p>
            </div>
            <div className="card-footer bg-transparent text-center border-0">
              <button type="button" className="btn btn-primary btn-sm" onClick={this.login.bind(this)}>
                <i className="fa fa-sign-in me-2" aria-hidden="true"></i>Login
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Logout;