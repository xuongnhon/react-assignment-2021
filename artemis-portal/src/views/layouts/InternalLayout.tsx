import React from 'react';
import { Suspense, lazy } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Header from '../Header';
import Loading from '../../components/Loading';
import { AppContext } from '../../AppContext';
const Dashboard = lazy(() => import('../Dashboard'));
const TaskList = lazy(() => import('../tasks/TaskList'));
const CreateTask = lazy(() => import('../tasks/CreateTask'));
const EditTask = lazy(() => import('../tasks/EditTask'));
const UserList = lazy(() => import('../users/UserList'));
const CreateUser = lazy(() => import('../users/CreateUser'));
const CreateUserSchedule = lazy(() => import('../users/CreateUserSchedule'));
const ScheduleList = lazy(() => import('../schedules/ScheduleList'));
const EditSchedule = lazy(() => import('../schedules/EditSchedule'));

class InternalLayout extends React.Component {
  render() {
    const { authenticationModel } = this.context;

    return (
      <>
        {authenticationModel.isAuthenticated
          ? (
            <>
              <Header />

              <main className="container">
                <Suspense fallback={<Loading />}>
                  <Switch>
                    <Route exact path="/internal/dashboard" component={Dashboard} />
                    <Route exact path="/internal/tasks" component={TaskList} />
                    <Route exact path="/internal/tasks/create" component={CreateTask} />
                    <Route exact path="/internal/tasks/edit/:id" component={EditTask} />
                    <Route exact path="/internal/users" component={UserList} />
                    <Route exact path="/internal/users/create" component={CreateUser} />
                    <Route exact path="/internal/users/:id/create-schedule" component={CreateUserSchedule} />
                    <Route exact path="/internal/schedules" component={ScheduleList} />
                    <Route exact path="/internal/schedules/edit/:id" component={EditSchedule} />
                    <Redirect from="*" to="/not-found" />
                  </Switch>
                </Suspense>
              </main>
            </>
          )
          : (
            <Redirect to='/public/login' />
          )}
      </>
    );
  }
}

InternalLayout.contextType = AppContext;

export default InternalLayout;
