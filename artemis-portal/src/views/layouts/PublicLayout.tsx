import { Suspense, lazy } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Loading from '../../components/Loading';
const Login = lazy(() => import('../Login'));

function PublicLayout() {
  return (
    <>
      <main className="container">
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact path="/public/login" component={Login} />
            <Redirect from="*" to="/not-found" />
          </Switch>
        </Suspense>
      </main>
    </>
  );
}

export default PublicLayout;
