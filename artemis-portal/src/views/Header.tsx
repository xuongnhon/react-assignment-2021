import React from 'react';
import { NavLink } from 'react-router-dom';
import { AppContext } from '../AppContext';
import { NavigationItem } from '../models/NavigationItem'

class Header extends React.Component<any> {
  async handleLogout() {
    const { dispatchAuthenticationModel } = this.context;
    dispatchAuthenticationModel({
      type: "LOGOUT"
    });
  }

  render() {
    const { authenticationModel } = this.context;
    const navigationItems: NavigationItem[] = [
      {
        name: "Dashboard",
        path: "/internal/dashboard"
      },
      {
        name: "Task",
        path: "/internal/tasks"
      }, {
        name: "User",
        path: "/internal/users"
      }, {
        name: "Schedule",
        path: "/internal/schedules"
      }
    ];

    return (
      <header className="p-3 mb-3 border-bottom">
        <div className="container">
          <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul className="nav nav-pills col-12 col-lg-auto me-lg-auto justify-content-center mb-md-0">
              {navigationItems.map((navigationItem) => (
                <li key={navigationItem.name}>
                  <NavLink className="nav-link" activeClassName="active" to={navigationItem.path}>{navigationItem.name}</NavLink>
                </li>
              ))}
            </ul>

            <div className="dropdown text-end">
              <span className="d-block link-dark text-decoration-none dropdown-toggle show-pointer" data-bs-toggle="dropdown"
                aria-expanded="false">
                {authenticationModel.loggedUser.name}
              </span>
              <ul className="dropdown-menu text-small">
                <li onClick={() => this.handleLogout()}>
                  <span className="dropdown-item show-pointer">Sign out</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

Header.contextType = AppContext;

export default Header;
