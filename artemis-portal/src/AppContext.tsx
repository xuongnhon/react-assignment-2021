import { createContext, useReducer } from 'react';
import { initialTasks, taskReducer } from './reducers/TaskReducer'
import { initialSchedules, scheduleReducer } from './reducers/ScheduleReducer'
import { initialUsers, userReducer } from './reducers/UserReducer'
import { initAuthenticationModel, authenticationModelReducer } from './reducers/AuthReducer'

const AppContext = createContext<any>(null);

const AppContextProvider = (props: any) => {
  const [tasks, dispatchTasks] = useReducer(taskReducer, initialTasks);
  const [users, dispatchUsers] = useReducer(userReducer, initialUsers);
  const [schedules, dispatchSchedules] = useReducer(scheduleReducer, initialSchedules);
  const [authenticationModel, dispatchAuthenticationModel] = useReducer(authenticationModelReducer, initAuthenticationModel);

  const value = {
    tasks,
    dispatchTasks,
    schedules,
    dispatchSchedules,
    users,
    dispatchUsers,
    authenticationModel,
    dispatchAuthenticationModel
  };

  return (
    <AppContext.Provider value={value}> {props.children} </AppContext.Provider>
  );
};

export { AppContext, AppContextProvider }
