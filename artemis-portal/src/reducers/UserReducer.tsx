import { User } from '../models/User';

const initialUsers: User[] = [
  {
    id: 1,
    name: "Nhon Vuong",
    role: "Admin",
    username: "admin",
    password: "P@ssw0rd"
  }
];

const userReducer = (state: User[], action: any) => {
  switch (action.type) {
    case 'ADD_USER':
      {
        const userId = state.length === 0
          ? 1
          : Math.max(...state.map((user: User) => user.id)) + 1;

        const newUser: User = {
          id: userId,
          name: action.user.name,
          role: action.user.role,
          username: action.user.username,
          password: action.user.password
        };
        return [...state, newUser];
      }
    case 'EDIT_USER':
      {
        const user = state.find((user: User) => user.id === action.user.id);

        if (user) {
          user.name = action.user.name;
          user.role = action.user.role;
        }

        return [...state];
      }
    case 'DELETE_USER':
      {
        const newState = state.filter((user: User) => {
          return user.id !== action.id
        });

        return [...newState];
      }
    default:
      throw new Error();
  }
};

export { initialUsers, userReducer }
