import { Task } from '../models/Task';
import { TaskStatus } from '../enums/TaskStatus';

const initialTasks: Task[] = [
  {
    id: 1,
    title: "Task 1",
    description: "Task 1 Description",
    status: TaskStatus.New
  },
  {
    id: 2,
    title: "Task 2",
    description: "Task 2 Description",
    status: TaskStatus.InProgress
  },
  {
    id: 3,
    title: "Task 3",
    description: "Task 3 Description",
    status: TaskStatus.Done
  },
  {
    id: 4,
    title: "Task 4",
    description: "Task 4 Description",
    status: TaskStatus.Done
  }
];

const taskReducer = (state: Task[], action: any) => {
  switch (action.type) {
    case 'ADD_TASK':
      {
        const taskId = state.length === 0
          ? 1
          : Math.max(...state.map((task: Task) => task.id)) + 1;

        const newTask: Task = {
          id: taskId,
          title: action.task.title,
          description: action.task.description,
          status: action.task.status
        };
        return [...state, newTask];
      }
    case 'EDIT_TASK':
      {
        const task = state.find((task: Task) => task.id === action.task.id);

        if (task) {
          task.title = action.task.title;
          task.description = action.task.description;
          task.status = action.task.status;
        }

        return [...state];
      }
    case 'DELETE_TASK':
      {
        const newState = state.filter((task: Task) => {
          return task.id !== action.id
        });

        return [...newState];
      }
    default:
      throw new Error();
  }
};

export { initialTasks, taskReducer }
