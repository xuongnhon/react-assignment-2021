import { AuthenticationModel } from '../models/AuthenticationModel';

const initAuthenticationModel: AuthenticationModel = {
  isAuthenticated: false,
  loggedUser: undefined
};

const authenticationModelReducer = (state: AuthenticationModel, action: any) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        isAuthenticated: true,
        loggedUser: { ...action.user }
      };
    case 'LOGOUT':
      return {
        isAuthenticated: false,
        loggedUser: undefined
      };;
    default:
      throw new Error();
  }
};

export { initAuthenticationModel, authenticationModelReducer }
