import moment from 'moment';
import { Schedule } from '../models/Schedule';

const initialSchedules: Schedule[] = [
  {
    id: 1,
    title: "Line Meeting",
    creator: {
      id: 1,
      name: "Nhon Vuong",
      role: "Admin",
    },
    description: "Discuss PAS",
    location: "HCM",
    startDate: moment().toDate(),
    endDate: moment().add(60, 'minutes').toDate()
  }
];

const scheduleReducer = (state: Schedule[], action: any) => {
  switch (action.type) {
    case 'ADD_SCHEDULE':
      {
        const scheduleId = state.length === 0
          ? 1
          : Math.max(...state.map((schedule: Schedule) => schedule.id)) + 1;

        const newSchedule: Schedule = {
          id: scheduleId,
          title: action.schedule.title,
          creator: action.schedule.creator,
          description: action.schedule.description,
          location: action.schedule.location,
          startDate: action.schedule.startDate,
          endDate: action.schedule.endDate
        };
        return [...state, newSchedule];
      }
    case 'EDIT_SCHEDULE':
      {
        const schedule = state.find((schedule: Schedule) => schedule.id === action.schedule.id);

        if (schedule) {
          schedule.title = action.schedule.title;
          schedule.creator = action.schedule.creator;
          schedule.description = action.schedule.description;
          schedule.location = action.schedule.location;
          schedule.startDate = action.schedule.startDate;
          schedule.endDate = action.schedule.endDate;
        }

        return [...state];
      }
    case 'DELETE_SCHEDULE':
      {
        const newState = state.filter((schedule: Schedule) => {
          return schedule.id !== action.id
        });

        return [...newState];
      }
    default:
      throw new Error();
  }
};

export { initialSchedules, scheduleReducer }
