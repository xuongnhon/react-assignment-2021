export class NavigationItem {
  name: string;
  path: string;
}
