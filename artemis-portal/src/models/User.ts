export class User {
  id: number;
  name: string;
  role: string;
  username?: string | undefined | null;
  password?: string | undefined | null;
}
