import { User } from './User';

export class Schedule {
  id: number;
  title: string;
  creator: User;
  description: string;
  location: string;
  startDate: Date;
  endDate: Date;
}
