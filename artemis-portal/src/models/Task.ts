import { TaskStatus } from '../enums/TaskStatus';

export class Task {
  id: number;
  title: string;
  description: string;
  status: number = TaskStatus.New;
}
