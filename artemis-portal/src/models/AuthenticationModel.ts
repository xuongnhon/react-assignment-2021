import { User } from './User';

export class AuthenticationModel {
  isAuthenticated: boolean;
  loggedUser: User | undefined | null;
}
